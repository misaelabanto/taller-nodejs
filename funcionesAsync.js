function algoAsincrono() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('Ya terminé');
    }, 2000);
  });
}

function divide(a, b) {
	return new Promise((resolve, reject) => {
		if(b === 0) {
			reject('No se puede dividir entre cero');
		} else {
			resolve(a/b);
		}
	});
}

// async function imprimirResultadoAsincrono() {
// 	let resultado = await algoAsincrono();
// 	console.log(resultado);
// }

algoAsincrono()
	.then(resultado => {
		console.log(resultado);
	})
	.catch(err => {
		console.error(err);
	});

divide(10, 5)
	.then(resultado => console.log(resultado))
	.catch(err => console.error(err));

// imprimirResultadoAsincrono();
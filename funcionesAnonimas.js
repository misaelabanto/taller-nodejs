const obtenerPromedioPracticas = function (alumno){
  let notas = alumno.notas;
  let minimo = 20;
	let sum = 0
	notas.pcs.forEach(function(pc) {
		if(pc < minimo) minimo = pc;
    sum += pc;
	});
  return (sum - minimo)/3;
};

const obtenerPromedioPracticasArrow = (alumno) => {
	let notas = alumno.notas;
  let minimo = 20;
	let sum = 0
	notas.pcs.forEach(pc => {
		if(pc < minimo) minimo = pc;
    sum += pc;
	});
  return (sum - minimo)/3;
};

let alumno = {
	nombre: 'Misael Abanto',
	notas: {
		pcs: [14, 10, 9, 11],
		examenes: {
			parcial: 11,
			final: 12
		}
	}
};

console.log(obtenerPromedioPracticas(alumno));

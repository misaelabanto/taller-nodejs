function obtenerPromedioPracticas(alumno){
	let notas = alumno.notas;
	let minimo = 20;
	let sum = 0
	for(let pc of notas.pcs){
		if(pc < minimo) minimo = pc;
		sum += pc;
	}
	return (sum - minimo)/3;
}

let alumno = {
	nombre: 'Misael Abanto',
	notas: {
		pcs: [14, 10, 9, 11],
		examenes: {
			parcial: 11,
			final: 12
		}
	}
};

console.log('El promedio de prácticas de', alumno.nombre, 'es:', obtenerPromedioPracticas(alumno));
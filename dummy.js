function padre(foo, myCallback){
	console.log("foo", foo);
	myCallback(foo);
}

const myCallbackLocal = (bar)=>{
	console.log("Llamando a mi callback", bar)
}

padre("fooValue", myCallbackLocal);
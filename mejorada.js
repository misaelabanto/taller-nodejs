const http = require('http');
const url = require('url');

http.createServer((request, response) => {
	let query = url.parse(request.url, true).query;
	response.writeHead(200, {
		'Content-Type': 'application/json'
	});
	response.end(JSON.stringify({
		respuesta: Number(query.a) + Number(query.b)
	}));
}).listen(3000);

console.log('Server running on port 3000');
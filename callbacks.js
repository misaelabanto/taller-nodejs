const fs = require('fs');

function callback2(){
	console.log("llamando a callback 2")
}
function callbackArchivoGuardado(err) {
	if (err) console.error(err);
	else console.log('Archivo guardado');
	callback2();
}

let promedioPracticas = 11.67;
let practicas = [8, 4, 15, 12]
//fs.writeFile('reporteNotas.txt', 'Promedio de prácticas:' + promedioPracticas, callbackArchivoGuardado);

fs.writeFile('reporteNotas.txt',
	'Promedio de prácticas:' + promedioPracticas + '\n' + practicas.join(", "),
	(err) => {
		
		if (err) console.error(err); // equivalente: cerr en C++
		else console.log('Archivo guardado');
	});
class Notas {
	constructor(pcs, examenes) {
		this.pcs = pcs;
		this.examenes = examenes;
	}
}

class Alumno {
	constructor(nombre, notas) {
		this.nombre = nombre;
		this.notas = notas;
	}
	static getNombreClase() {
		return 'Alumno';
	}
	obtenerPromedioPracticas(){
		let minimo = 20, sum = 0;
		this.notas.pcs.forEach(function(pc) {
			if(pc < minimo) minimo = pc;
			sum += pc;
		});
		return (sum - minimo)/3;
	};
}

let notas = new Notas([14, 10, 9, 11], { parcial: 11, final: 12 });
let alumno = new Alumno('Misael Abanto', notas);
console.log(alumno.obtenerPromedioPracticas());

console.log(alumno);
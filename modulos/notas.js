// notas.js
const util = require('./util');
const alumno = require('./alumno');

console.log('Promedio de prácticas:', util.calcularPromedioPracticas(alumno).toFixed(2));
console.log('Promedio final:', util.obtenerPromedioFinal(alumno).toFixed(2));
function calcularPromedioPracticas(alumno) {
	let notas = alumno.notas;
  let minimo = 20;
	let sum = 0
	notas.pcs.forEach(function(pc) {
		if(pc < minimo) minimo = pc;
    sum += pc;
	});
  return (sum - minimo)/3;
}

function obtenerPromedioFinal(alumno) {
	let promedioPracticas = calcularPromedioPracticas(alumno);
	return (promedioPracticas + alumno.notas.examenes.parcial + alumno.notas.examenes.final) / 3;
}

module.exports = {
	calcularPromedioPracticas,
	obtenerPromedioFinal
}
// alumno.js

let alumno = {
	nombre: 'Misael Abanto',
	notas: {
		pcs: [14, 10, 9, 11],
		examenes: {
			parcial: 11,
			final: 12
		}
	}
};

module.exports = alumno;
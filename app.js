const express = require('express');

const app = express();
app.use(express.json());
app.use(express.urlencoded());

app.get('/', (req, res) => {
	res.send('welcome to Express a=' + req.query.a);
});

const parseAllInt = arr => arr.map(elem => parseInt(elem));

app.get('/suma', (req, res) => {
	let {a, b} = req.query;
	let [aInt, bInt] = parseAllInt([a,b]);
	let resultado = aInt + bInt;
	res.send('La suma es: ' + resultado);
});

app.get('/resta', (req, res) => {
	let {a, b} = req.query;
	let [aInt, bInt] = parseAllInt([a,b]);
	let resultado = aInt - bInt;
	res.send('La resta es: ' + resultado);
});

// Params
app.get('/multiplicacion/:a/:b', (req, res) => {
	let {a, b} = req.params;
	let [aInt, bInt] = parseAllInt([a,b]);
	let resultado = aInt * bInt;
	res.send('La multiplicación es: ' + resultado);
});

app.post('/division', (req, res) => {
	let {a, b} = req.body;
	let [aInt, bInt] = parseAllInt([a,b]);
	let resultado = aInt / bInt;
	res.send('La división es: ' + resultado);
});

app.get('/saludo/:nombre', (req, res) => {
	let nombre = req.params.nombre;
	res.send(`<h5 style="color: green">Hola, ${nombre}</h5>`);
});

app.listen(3000, () => {
	console.log('Node app is running on port', 3000);
});
// son lo mismo 
// app.listen(3000, function() {
// 	console.log('Node app is running on port', 3000);
// });